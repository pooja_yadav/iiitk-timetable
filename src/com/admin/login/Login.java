package com.admin.login;

import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(description = "Validates user against data of database", urlPatterns = { "/adminloginservlet" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException { 
		try{ 
			LoginParameters facuser = new LoginParameters(); 
			String uname=request.getParameter("username");
			uname=uname.toUpperCase();
			facuser.setId(request.getParameter("username"));
			facuser.setPassword(request.getParameter("password"));
			facuser = LoginDAO.login(facuser); 
			if (facuser.isValid()){ 
				HttpSession session = request.getSession(true); 
				session. setMaxInactiveInterval(15*5);
				Date createTime = new Date(session.getCreationTime());    // Get session creation time.
				Date lastAccessTime = new Date(session.getLastAccessedTime());   // Get last access time of this web page.
				System.out.println("Current time " +createTime+"last accessed "+lastAccessTime);
				session.setAttribute("currentFacultySessionUser",facuser); //currentSessionUser is object of user
				response.sendRedirect("dashboard.jsp");
			}else 
				response.sendRedirect("invalidLogin.jsp"); //error page 
		}catch (Throwable theException){ 
				System.out.println(theException); 
		} 
	}
}

